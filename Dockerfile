FROM openjdk:15-jdk-slim
#WORKDIR application
ADD dependencies/ ./
ADD spring-boot-loader/ ./
ADD snapshot-dependencies/ ./
ADD application/ ./
ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher"]